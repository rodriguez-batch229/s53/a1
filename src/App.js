import "./App.css";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import AppNavbar from "./components/AppNavbar.js";

import Home from "./pages/Home.js";
import Courses from "./pages/Courses.js";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Error from "./pages/Error";

// React JS is a single page application (SPA)
// Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
// When a link is clicked, React JS changes the url of the application to mirror how HTML accesses its urls
// It renders the component executing the function component and it's expressions
// After rendering it mounts the component displaying the elements
// Whenever a state is updated or changes are made with React JS, it rerenders the component
// Lastly, when a different page is loaded, it unmounts the component and repeats this process
// The updating of the user interface closely mirrors that of how HTML deals with page navigation with the exception that React JS does not reload the whole page

function App() {
   return (
      // Fragment: <> and </>
      /* Mount component and prepare output rendering */
      <>
         <Router>
            <AppNavbar />
            <Container>
               <Routes>
                  <Route exact path="/" element={<Home />} />
                  <Route exact path="/courses" element={<Courses />} />
                  <Route exact path="/login" element={<Login />} />
                  <Route exact path="/register" element={<Register />} />
                  <Route path="*" element={<Error />} />
               </Routes>
            </Container>
         </Router>
      </>
   );
}

export default App;
