import Banner from "../components/Banner.js";

export default function Home() {
   return (
      <>
         <Banner
            heading="Error 404"
            paragraph="Page Not Found"
            state="Back to Home Page"
            destination="/"
         />
      </>
   );
}
