// Destructuring components/modules for cleaner codebase
import { Button, Row, Col } from "react-bootstrap";

import { Link } from "react-router-dom";

export default function Banner(props) {
   return (
      <Row>
         <Col className="p-5 text-center">
            <h1>{props.heading}</h1>
            <p>{props.paragraph}</p>

            <Button variant="primary">
               <Link className="remove-linkStyle" to={props.destination}>
                  {props.state}
               </Link>
            </Button>
         </Col>
      </Row>
   );
}
